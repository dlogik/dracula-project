package dracula;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;

// Implementation of DraculaMove
public class MyDraculaMove implements DraculaMove {

    private String pastPlays;
    private List<PastPlay> pastPlaysList;
    private DracMap dracMap;
    // All characters' plays represented as individual queues/stacks, with most recent moves at the front
    private ArrayDeque<Location> gMoves, sMoves, hMoves, mMoves, dracMoves, dracTrail;
    
    /*
     * From the spec:
     *  - The Hunters can co-operate using messages to rid the world of Dracula as swiftly as possible.
     *  - Sadly Dracula gets to hear everything the Hunters say to each other. */
    private String[] message;
    
    public MyDraculaMove(String pastPlays, String[] message) {
    	this.message = message;

    	//pastPlays is a string representing everything that the hunters have observed happening in the game so far.  
    	//It lists all the plays made in the game so far by all players (including Dracula) from the earliest turn 
    	//(at the start of the string) to most recent turn (at the end of the string).
        this.pastPlays = pastPlays;
        this.pastPlaysList = new ArrayList<PastPlay>();
        gMoves = new ArrayDeque<Location>();
        sMoves = new ArrayDeque<Location>();
        hMoves = new ArrayDeque<Location>();
        mMoves = new ArrayDeque<Location>();
        dracMoves = new ArrayDeque<Location>();
        dracTrail = new ArrayDeque<Location>();
        dracMap = new DracMap();
        initPastPlayArray(pastPlays);
        initDracTrail();
        setHunterLocations();
    } 
    
    private void initPastPlayArray(String pastPlays) {
    	this.pastPlaysList.clear();
    	for (String s : pastPlays.split(" ")) {
    		switch(s.charAt(0)) {
    		case 'G':
    			gMoves.push(dracMap.locations.get(s.substring(1, 3)));
    			break;
    		case 'S':
    			sMoves.push(dracMap.locations.get(s.substring(1, 3)));
    			break;
    		case 'H':
    			hMoves.push(dracMap.locations.get(s.substring(1, 3)));
    			break;
    		case 'M':
    			mMoves.push(dracMap.locations.get(s.substring(1, 3)));
    			break;
    		case 'D':
    			this.pastPlaysList.add(new PastPlay(s));
    			dracMoves.push(dracMap.locations.get(s.substring(1, 3)));
    			break;
    		}
    	}
    }
    
    private void setHunterLocations() {
    	dracMap.hunterLocations.add(gMoves.peek());
    	dracMap.hunterLocations.add(sMoves.peek());
    	dracMap.hunterLocations.add(hMoves.peek());
    	dracMap.hunterLocations.add(hMoves.peek());
    }
    
    private void initDracTrail() {
    	ArrayDeque<Location> moves = dracMoves.clone();
    	// Add the 6 most recent dracula moves to the trail
    	for(int i = 0; i < 6; i++) {
    		
    		// Only add to the dracTail queue if dracMoves is not empty.
    		if (!moves.isEmpty()) {
    			dracTrail.add(moves.pop());
    		}  		
    	}
    }
    
    // Randomly selects a city for testing (returns city code)
    private String getCity() {
    	ArrayList<Location> possibleDestinations = dracMap.getPossibleDestinations(dracTrail);
    	ArrayDeque<Location> prioritisedDestinations = dracMap.prioritiseDestinations(possibleDestinations);
    	
    	if(prioritisedDestinations.size() < 1) {
    		return "TP";
    	}
    	
    	return prioritisedDestinations.peek().name;
    	/*
    	if(prioritisedDestinations.size() == 1) {
    		return prioritisedDestinations.pop().name;
    	}
    	if(possibleDestinations.size() > 1) {
    		int random = (int)(Math.random() * possibleDestinations.size());
    		return possibleDestinations.get(random).name;
    	}
    	else {
    		return "TP";
    	}
    	*/
    }
            
    // The getPlayAsString method in Dracula Move should return a string of length 2, being the location that Dracula wants to move to.
    // You should return HI if you are hiding, D1 if u are doubling back to current location, D2 if you are going back one city etc. 
    public String getPlayAsString() {
        PastPlay lastPlay =  this.pastPlaysList.get(this.pastPlaysList.size() - 1);
        // If this is the first move, start in Strasbourg (for now)
        if(dracTrail.size() == 0) {
        	return "ST";
        }
    
        return getCity();
    }
 
    public String getMessage() {
        // TODO Auto-generated method stub
        return "";
    }
 
}
