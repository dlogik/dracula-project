package dracula;

// First dracula implementation.
public class MyDraculaOne implements Dracula {

	String pastPlays;
	String[] messages;
	
	public MyDraculaOne(String pastPlays, String[] messages) {
		this.pastPlays = pastPlays;
		this.messages = messages;
	}

	public DraculaMove decideMove() {
		DraculaMove dracMove = new MyDraculaMove(pastPlays, messages);
				
		return dracMove;
	}
}
