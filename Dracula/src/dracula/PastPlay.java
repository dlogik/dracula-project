package dracula;

// This class takes a single play string in the constructor and converts it into a readable object.
public class PastPlay {
	
	// Character code.
	private char charCode;
	
	// 2 uppercase characters representing the new location of the hunter
	private String cityString;
	
	private Boolean trapPlaced;
	private Boolean vampPlaced;

	// Actions
	private Boolean vampMatured;
	private Boolean trapLeft;
		
	private Boolean doubleBackMove;
	private int doubleBackNumber;
	private Boolean teleportCastle;
	
	// Constructor takes a single play string (7 characters).
	// As dracula, we always know the location.
	public PastPlay(String initString) {
		// First character is player.
		this.charCode = initString.charAt(0);
		
		// Second two characters is new location of Dracula.
		this.cityString = initString.substring(1, 3);
		
		// Encounter Dracula placed
		this.trapPlaced = initString.charAt(3) == 'T';
		this.vampPlaced = initString.charAt(4) == 'V';
		
		// Actions
		this.trapLeft = initString.charAt(5) == 'M';
		this.vampMatured = initString.charAt(5) == 'V';
		
		loadDoubleBack();
	}
	
	private void loadDoubleBack() {
		if (cityString.charAt(0) == 'D') {
			this.doubleBackMove = true;
			this.doubleBackNumber = Integer.parseInt(cityString.substring(1,2));
		} else {
			this.doubleBackMove = false;
			this.doubleBackNumber = -1;
		}
	}
	
	/*		G = Lord Godalming (turns 1, 6, ...)
			S = Dr. Seward (turns 2, 7, ...)
			H = Van Helsing (turns 3, 8, ...)
			M = Mina Harker (turns 4, 9, ...)
			D = Dracula (turns 5, 10, ...) */
	public char getCharacterCode() {
		return this.charCode;
	}
	
	// True if we have discovered it was a hide move
	public Boolean hideMove() {
		return this.cityString == "HI";
	}
	
	// Double Back move
	public Boolean doubleBackMove() {
		return this.doubleBackMove;		
	}
	
	// Double back number.
	public int doubleBackNumber() {
		return this.doubleBackNumber;
	}
	
	// Teleport to Castle Dracula
	public Boolean teleportCastleMove() {
		return this.teleportCastle;
	}
	
	// City code as a string.
	public String getCurrentCityCode() {	
		return this.cityString;
	}
	
	// Encounter trap placed.
	public Boolean trapPlaced() {
		return this.trapPlaced;
	}
	
	// Encounter vampire placed
	public Boolean vampirePlaced() {
		return this.vampPlaced;
	}
	
	// Action - true a Vampire has matured.
	public Boolean vampireMatured() {
		return this.vampMatured;
	}
	
	// Action - true if a Trap has left the trail (malfunctions due to old age), and vanishes without a trace.
	public Boolean trapLeft() {
		return this.trapLeft;
	}
}
