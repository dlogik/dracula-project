package dracula;

import java.util.*;


public class DracMap {
	ArrayList<Path> paths;
	HashMap<String,Location> locations;
	ArrayList<Location> hunterLocations;
	ArrayList<Location> locationsTowardsHunters;
	Location currentLocation;
	
	
	public DracMap() {
		initMap();
		hunterLocations = new ArrayList<Location>();
		locationsTowardsHunters = new ArrayList<Location>();
	}
	
	public ArrayList<Location> getPossibleDestinations(ArrayDeque<Location> dracTrail) {
		// Creates a list of destinations from the current location 
		ArrayList<Location> possibleDestinations = new ArrayList<Location>();
		currentLocation = dracTrail.peek();
		boolean hideAvailable = true;
		boolean doubleBackAvailable = true;
		for(Location l : dracTrail) {
			if(l.name.matches("\\D[1-5]")) {
				doubleBackAvailable = false;
			}
			if(l.name.equals("HI")) {
				hideAvailable = false;
			}
		}
		for(Path path : currentLocation.incidentPaths) {
			possibleDestinations.add(opposite(currentLocation, path));
		}
				
		// Remove locations in the trail from list of destinations
		possibleDestinations.removeAll(dracTrail);
		
		// Add double back moves if available
		if(doubleBackAvailable) {
			ArrayDeque<Location> trail = dracTrail.clone();
			Location previousLocation;
			// double back to current location is available
			locations.put("D1", trail.pop());
			locations.get("D1").name = "D1";
			possibleDestinations.add(locations.get("D1"));
			for(int i = 2; i < 6; i++) {
				if(trail.size() == 0) {
					break;
				}
				previousLocation = trail.pop();
				if(areAdjacent(currentLocation, previousLocation)) {
					locations.put("D"+i, previousLocation);
					locations.get("D"+i).name = "D"+i;
					possibleDestinations.add(locations.get("D"+i));
				}
			}
		}
		// Add hide move if available
		if(hideAvailable) {
			locations.put("HI", currentLocation);
			locations.get("HI").name = "HI";
			possibleDestinations.add(locations.get("HI"));
		}
		
		return possibleDestinations;
	}
	
	public ArrayDeque<Location> prioritiseDestinations(ArrayList<Location> destinations) {
		ArrayDeque<Location> prioritisedDestinations = new ArrayDeque<Location>();
		if(destinations == null) {
			return null;
		}
		findPathsToHunters();
		while(destinations.size() > 0) {
			// Make destinations that lead towards hunters the lowest priority
			for(Location destination : destinations) {
				if(locationsTowardsHunters.contains(destination)) {
					prioritisedDestinations.push(destination);
				}
			}
			destinations.removeAll(prioritisedDestinations);
			
			// Make sea locations the next lowest priority
			for(Location destination : destinations) {
				if(destination.locationType.equals("sea")) {
					prioritisedDestinations.push(destination);
				}
			}
			destinations.removeAll(prioritisedDestinations);
			
			// Make double-back moves the next lowest priority
			for(Location destination : destinations) {
				if(destination == locations.get("D1")) {
					prioritisedDestinations.push(destination);
				}
			}
			destinations.removeAll(prioritisedDestinations);
			
			for(Location destination : destinations) {
				if(destination == locations.get("D2")) {
					prioritisedDestinations.push(destination);
				}
			}
			destinations.removeAll(prioritisedDestinations);
			
			for(Location destination : destinations) {
				if(destination == locations.get("D3")) {
					prioritisedDestinations.push(destination);
				}
			}
			destinations.removeAll(prioritisedDestinations);
			
			for(Location destination : destinations) {
				if(destination == locations.get("D4")) {
					prioritisedDestinations.push(destination);
				}
			}
			destinations.removeAll(prioritisedDestinations);
			
			for(Location destination : destinations) {
				if(destination == locations.get("D5")) {
					prioritisedDestinations.push(destination);
				}
			}
			destinations.removeAll(prioritisedDestinations);
			
			// Make hide moves the next lowest priority
			for(Location destination : destinations) {
				if(destination == locations.get("HI")) {
					prioritisedDestinations.push(destination);
				}
			}
			destinations.removeAll(prioritisedDestinations);
			
			// Add the remaining city-moves that are not towards hunters as the highest priority
			for(Location destination : destinations) {
				prioritisedDestinations.push(destination);
			}
			destinations.removeAll(prioritisedDestinations);
			
		}
		return prioritisedDestinations;
		
	}
	
	private void findPathsToHunters() {
		HashMap<Location,PathCost> lcpKnown = new HashMap<Location,PathCost>();
		ArrayList<PathCost> dijkstraTable = new ArrayList<PathCost>();
		PathCost leastCostPath;
		if(hunterLocations.isEmpty()) {
			return;
		}
		
		// Initialisation step of Dijkstra's algorithm
		for(Location location : locations.values()) {
			if(location == currentLocation) {
				continue;
			}
			else {
				dijkstraTable.add(new PathCost(location, currentLocation));
			}
		}

		// Loop step of Dijkstra's algorithm
		while(dijkstraTable.size() > 0) {
			// Find next least cost link
			leastCostPath = null;
			for(PathCost pathCost : dijkstraTable) {
				if (leastCostPath == null) {
					leastCostPath = pathCost;
				}
				else if (pathCost.cost < leastCostPath.cost)
					leastCostPath = pathCost;
			}
			// Add least cost link to list of known least costs
			lcpKnown.put(leastCostPath.location, leastCostPath);
			// Remove new known least cost link from Dijkstra table
			dijkstraTable.remove(leastCostPath);
			
			// Update link costs in Dijkstra table
			for(PathCost pathCost : dijkstraTable) {
				pathCost.updateCost(leastCostPath);
			}
		}
		
		// Determine next hop from source for each destination
		for(Location destination : hunterLocations) {
			if(destination == currentLocation) {
				continue;
			}
			else {
				Location nextHop = destination;
				while(nextHop != null && lcpKnown.get(nextHop).predecessor != currentLocation) {
					nextHop = lcpKnown.get(nextHop).predecessor;
				}
				locationsTowardsHunters.add(nextHop);
			}
		}

	}
	
	public Location opposite(Location location, Path path) {
		// Returns the location at the opposite end of the given path from the given location
		if(path.location1 == location) {
			return path.location2;
		}
		if(path.location2 == location) {
			return path.location1;
		}
		return null;
	}
	
	private boolean areAdjacent(Location location1, Location location2) {
		boolean areAdjacent = false;
		for(Path path : location1.incidentPaths) {
			if(opposite(location1, path) == location2) {
				areAdjacent = true;
			}
		}
		return areAdjacent;
	}
	
	public static Path getPath(Location location1, Location location2) {
		if (location1 == location2) {
			return null;
		}
		// Iterate over whichever router has a smaller degree
		if (location1.incidentPaths.size() < location2.incidentPaths.size()) {
			for (Path p : location1.incidentPaths) {
				if (p.location1==location2||p.location2==location2) {
					return p;
				}
			}
		}
		else {
			for (Path p : location2.incidentPaths) {
				if (p.location1==location1||p.location2==location1) {
					return p;
				}
			}
		}
		return null;
	}

	
	public void printMap() {
		System.out.format("%-15s%-15s%-15s\n", "Link Type", "From", "To");
		
		for (Path p : paths) {
			System.out.format("%-15s%-15s%-15s\n", p.location1.locationType, p.location1.name, p.location2.name);
		}
		
	}
	
	private void initMap() {
		// Creates the map and fills it with the locations and paths
		locations = new HashMap<String,Location>(70);
		paths = new ArrayList<Path>(200);
		
		String[] cityNames = 	 {  "JM", "AL", "AM", "AT", "BA", "BI", "BE",
									"BR", "BO", "BU", "BC", "BD", "CA", "CG", "CD", "CF", "CO",
									"CN", "DU", "ED", "FL", "FR", "GA", "GW", "GE", "GO", "GR",
									"HA", "KL", "LE", "LI", "LS", "LV", "LO", "MA", "MN", "MR",
									"MI", "MU", "NA", "NP", "NU", "PA", "PL", "PR", "RO", "SA",
									"SN", "SR", "SJ", "SO", "ST", "SW", "SZ", "TO", "VA", "VR",
									"VE", "VI", "ZA", "ZU", "NS", "EC", "IS", "AO", "BB", "MS",
									"TS", "IO", "AS", "BS" };
		
		String[] seaNames =		 {"NS", "EC", "IS", "AO", "BB", "MS", "TS", "IO", "AS", "BS"};
		
		String[] specialNames =  {"HI", "D1", "D2", "D3", "D4", "D5"};
		
		// Create city objects in the map with the name as the key
		for(String city : cityNames) {
			locations.put(city, new Location(city, "city"));
		}
		
		// Create sea objects in the map with the name as the key
		for(String sea : seaNames) {
			locations.put(sea, new Location(sea, "sea"));
		}
		
		// Create special locations
		for(String special : specialNames) {
			locations.put(special, new Location(special, "special"));
		}
		locations.put("TP", locations.get("CD"));
		
		
        // Create path objects that reference the incident locations
		paths.add(new Path(locations.get("CD"), locations.get("GA")));
		paths.add(new Path(locations.get("CD"), locations.get("KL")));
		paths.add(new Path(locations.get("KL"), locations.get("BD")));
		paths.add(new Path(locations.get("KL"), locations.get("SZ")));
		paths.add(new Path(locations.get("KL"), locations.get("BE")));
		paths.add(new Path(locations.get("KL"), locations.get("BC")));
		paths.add(new Path(locations.get("KL"), locations.get("GA")));
		paths.add(new Path(locations.get("GA"), locations.get("CN")));
		paths.add(new Path(locations.get("GA"), locations.get("BC")));
		paths.add(new Path(locations.get("BC"), locations.get("CN")));
		paths.add(new Path(locations.get("BC"), locations.get("SO")));
		paths.add(new Path(locations.get("BC"), locations.get("BE")));
		paths.add(new Path(locations.get("SO"), locations.get("VR")));
		paths.add(new Path(locations.get("SO"), locations.get("SA")));
		paths.add(new Path(locations.get("SO"), locations.get("BE")));
		paths.add(new Path(locations.get("SO"), locations.get("SJ")));
		paths.add(new Path(locations.get("SO"), locations.get("VA")));
		paths.add(new Path(locations.get("SZ"), locations.get("BE")));
		paths.add(new Path(locations.get("SZ"), locations.get("BD")));
		paths.add(new Path(locations.get("SZ"), locations.get("ZA")));
		paths.add(new Path(locations.get("SZ"), locations.get("JM")));
		paths.add(new Path(locations.get("BD"), locations.get("VI")));
		paths.add(new Path(locations.get("BD"), locations.get("ZA")));
		paths.add(new Path(locations.get("VI"), locations.get("ZA")));
		paths.add(new Path(locations.get("VI"), locations.get("PR")));
		paths.add(new Path(locations.get("VI"), locations.get("MU")));
		paths.add(new Path(locations.get("ZA"), locations.get("MU")));
		paths.add(new Path(locations.get("ZA"), locations.get("SJ")));
		paths.add(new Path(locations.get("ZA"), locations.get("JM")));
		paths.add(new Path(locations.get("CN"), locations.get("VR")));
		paths.add(new Path(locations.get("SA"), locations.get("VA")));
		paths.add(new Path(locations.get("VA"), locations.get("AT")));
		paths.add(new Path(locations.get("VA"), locations.get("SJ")));
		paths.add(new Path(locations.get("SJ"), locations.get("JM")));
		paths.add(new Path(locations.get("SJ"), locations.get("BE")));
		paths.add(new Path(locations.get("BE"), locations.get("JM")));
		paths.add(new Path(locations.get("VE"), locations.get("MU")));
		paths.add(new Path(locations.get("VE"), locations.get("MI")));
		paths.add(new Path(locations.get("VE"), locations.get("FL")));
		paths.add(new Path(locations.get("VE"), locations.get("GO")));
		paths.add(new Path(locations.get("FL"), locations.get("GO")));
		paths.add(new Path(locations.get("FL"), locations.get("RO")));
		paths.add(new Path(locations.get("RO"), locations.get("BI")));
		paths.add(new Path(locations.get("RO"), locations.get("NP")));
		paths.add(new Path(locations.get("NP"), locations.get("BI")));
		paths.add(new Path(locations.get("CF"), locations.get("MR")));
		paths.add(new Path(locations.get("CF"), locations.get("TO")));
		paths.add(new Path(locations.get("CF"), locations.get("BO")));
		paths.add(new Path(locations.get("CF"), locations.get("NA")));
		paths.add(new Path(locations.get("CF"), locations.get("PA")));
		paths.add(new Path(locations.get("CF"), locations.get("GE")));
		paths.add(new Path(locations.get("PA"), locations.get("CF")));
		paths.add(new Path(locations.get("PA"), locations.get("NA")));
		paths.add(new Path(locations.get("PA"), locations.get("LE")));
		paths.add(new Path(locations.get("PA"), locations.get("BU")));
		paths.add(new Path(locations.get("PA"), locations.get("ST")));
		paths.add(new Path(locations.get("PA"), locations.get("GE")));
		paths.add(new Path(locations.get("BU"), locations.get("PA")));
		paths.add(new Path(locations.get("BU"), locations.get("LE")));
		paths.add(new Path(locations.get("BU"), locations.get("AM")));
		paths.add(new Path(locations.get("BU"), locations.get("CO")));
		paths.add(new Path(locations.get("BU"), locations.get("ST")));
		paths.add(new Path(locations.get("AM"), locations.get("BU")));
		paths.add(new Path(locations.get("AM"), locations.get("CO")));
		paths.add(new Path(locations.get("HA"), locations.get("BR")));
		paths.add(new Path(locations.get("HA"), locations.get("LI")));
		paths.add(new Path(locations.get("HA"), locations.get("CO")));
		paths.add(new Path(locations.get("CO"), locations.get("ST")));
		paths.add(new Path(locations.get("CO"), locations.get("BU")));
		paths.add(new Path(locations.get("CO"), locations.get("AM")));
		paths.add(new Path(locations.get("CO"), locations.get("HA")));
		paths.add(new Path(locations.get("CO"), locations.get("LI")));
		paths.add(new Path(locations.get("CO"), locations.get("FR")));
		paths.add(new Path(locations.get("ST"), locations.get("GE")));
		paths.add(new Path(locations.get("ST"), locations.get("PA")));
		paths.add(new Path(locations.get("ST"), locations.get("BU")));
		paths.add(new Path(locations.get("ST"), locations.get("CO")));
		paths.add(new Path(locations.get("ST"), locations.get("FR")));
		paths.add(new Path(locations.get("ST"), locations.get("NU")));
		paths.add(new Path(locations.get("ST"), locations.get("MU")));
		paths.add(new Path(locations.get("ST"), locations.get("ZU")));
		paths.add(new Path(locations.get("GE"), locations.get("CF")));
		paths.add(new Path(locations.get("GE"), locations.get("PA")));
		paths.add(new Path(locations.get("GE"), locations.get("ST")));
		paths.add(new Path(locations.get("GE"), locations.get("ZU")));
		paths.add(new Path(locations.get("GE"), locations.get("MR")));
		paths.add(new Path(locations.get("MR"), locations.get("TO")));
		paths.add(new Path(locations.get("MR"), locations.get("CF")));
		paths.add(new Path(locations.get("MR"), locations.get("GE")));
		paths.add(new Path(locations.get("MR"), locations.get("ZU")));
		paths.add(new Path(locations.get("MR"), locations.get("MI")));
		paths.add(new Path(locations.get("MR"), locations.get("GO")));
		paths.add(new Path(locations.get("BR"), locations.get("HA")));
		paths.add(new Path(locations.get("BR"), locations.get("LI")));
		paths.add(new Path(locations.get("BR"), locations.get("PR")));
		paths.add(new Path(locations.get("LI"), locations.get("NU")));
		paths.add(new Path(locations.get("LI"), locations.get("FR")));
		paths.add(new Path(locations.get("LI"), locations.get("CO")));
		paths.add(new Path(locations.get("LI"), locations.get("HA")));
		paths.add(new Path(locations.get("LI"), locations.get("BR")));
		paths.add(new Path(locations.get("FR"), locations.get("NU")));
		paths.add(new Path(locations.get("FR"), locations.get("ST")));
		paths.add(new Path(locations.get("FR"), locations.get("CO")));
		paths.add(new Path(locations.get("FR"), locations.get("LI")));
		paths.add(new Path(locations.get("ZU"), locations.get("MI")));
		paths.add(new Path(locations.get("ZU"), locations.get("MR")));
		paths.add(new Path(locations.get("ZU"), locations.get("GE")));
		paths.add(new Path(locations.get("ZU"), locations.get("ST")));
		paths.add(new Path(locations.get("ZU"), locations.get("MU")));
		paths.add(new Path(locations.get("MI"), locations.get("GO")));
		paths.add(new Path(locations.get("MI"), locations.get("MR")));
		paths.add(new Path(locations.get("MI"), locations.get("ZU")));
		paths.add(new Path(locations.get("MI"), locations.get("MU")));
		paths.add(new Path(locations.get("MI"), locations.get("VE")));
		paths.add(new Path(locations.get("GO"), locations.get("FL")));
		paths.add(new Path(locations.get("GO"), locations.get("MR")));
		paths.add(new Path(locations.get("GO"), locations.get("MI")));
		paths.add(new Path(locations.get("GO"), locations.get("VE")));
		paths.add(new Path(locations.get("PR"), locations.get("BR")));
		paths.add(new Path(locations.get("PR"), locations.get("NU")));
		paths.add(new Path(locations.get("PR"), locations.get("VI")));
		paths.add(new Path(locations.get("NU"), locations.get("MU")));
		paths.add(new Path(locations.get("NU"), locations.get("ST")));
		paths.add(new Path(locations.get("NU"), locations.get("FR")));
		paths.add(new Path(locations.get("NU"), locations.get("LI")));
		paths.add(new Path(locations.get("NU"), locations.get("PR")));
		paths.add(new Path(locations.get("MU"), locations.get("VE")));
		paths.add(new Path(locations.get("MU"), locations.get("MI")));
		paths.add(new Path(locations.get("MU"), locations.get("ZU")));
		paths.add(new Path(locations.get("MU"), locations.get("ST")));
		paths.add(new Path(locations.get("MU"), locations.get("NU")));
		paths.add(new Path(locations.get("MU"), locations.get("VI")));
		paths.add(new Path(locations.get("MU"), locations.get("ZA")));
		paths.add(new Path(locations.get("ED"), locations.get("MN")));
		paths.add(new Path(locations.get("MN"), locations.get("LV")));
		paths.add(new Path(locations.get("MN"), locations.get("LO")));
		paths.add(new Path(locations.get("LO"), locations.get("SW")));
		paths.add(new Path(locations.get("LO"), locations.get("PL")));
		paths.add(new Path(locations.get("LV"), locations.get("SW")));
		paths.add(new Path(locations.get("DU"), locations.get("GW")));
		paths.add(new Path(locations.get("LE"), locations.get("NA")));
		paths.add(new Path(locations.get("LE"), locations.get("PA")));
		paths.add(new Path(locations.get("PA"), locations.get("NA")));
		paths.add(new Path(locations.get("NA"), locations.get("CF")));
		paths.add(new Path(locations.get("CF"), locations.get("BO")));
		paths.add(new Path(locations.get("CF"), locations.get("TO")));
		paths.add(new Path(locations.get("TO"), locations.get("BO")));
		paths.add(new Path(locations.get("BO"), locations.get("SR")));
		paths.add(new Path(locations.get("TO"), locations.get("SR")));
		paths.add(new Path(locations.get("TO"), locations.get("BA")));
		paths.add(new Path(locations.get("SR"), locations.get("SN")));
		paths.add(new Path(locations.get("SR"), locations.get("AL")));
		paths.add(new Path(locations.get("SR"), locations.get("MA")));
		paths.add(new Path(locations.get("SN"), locations.get("MA")));
		paths.add(new Path(locations.get("MA"), locations.get("AL")));
		paths.add(new Path(locations.get("MA"), locations.get("LS")));
		paths.add(new Path(locations.get("MA"), locations.get("CA")));
		paths.add(new Path(locations.get("MA"), locations.get("GR")));
		paths.add(new Path(locations.get("AL"), locations.get("GR")));
		paths.add(new Path(locations.get("LS"), locations.get("SN")));
		paths.add(new Path(locations.get("CA"), locations.get("LS")));
		paths.add(new Path(locations.get("CA"), locations.get("GR")));
		paths.add(new Path(locations.get("SR"), locations.get("BA")));
		
		paths.add(new Path(locations.get("CN"), locations.get("BS")));
		paths.add(new Path(locations.get("VR"), locations.get("BS")));
		paths.add(new Path(locations.get("SA"), locations.get("IO")));
		paths.add(new Path(locations.get("AT"), locations.get("IO")));
		paths.add(new Path(locations.get("VA"), locations.get("IO")));
		paths.add(new Path(locations.get("VE"), locations.get("AS")));
		paths.add(new Path(locations.get("BI"), locations.get("AS")));
		paths.add(new Path(locations.get("CG"), locations.get("TS")));
		paths.add(new Path(locations.get("RO"), locations.get("TS")));
		paths.add(new Path(locations.get("NP"), locations.get("TS")));
		paths.add(new Path(locations.get("CG"), locations.get("MS")));
		paths.add(new Path(locations.get("MS"), locations.get("TS")));
		paths.add(new Path(locations.get("IO"), locations.get("TS")));
		paths.add(new Path(locations.get("IO"), locations.get("AS")));
		paths.add(new Path(locations.get("ED"), locations.get("NS")));
		paths.add(new Path(locations.get("LS"), locations.get("NS")));
		paths.add(new Path(locations.get("LV"), locations.get("AO")));
		paths.add(new Path(locations.get("GW"), locations.get("AO")));
		paths.add(new Path(locations.get("DU"), locations.get("IS")));
		paths.add(new Path(locations.get("SW"), locations.get("IS")));
		paths.add(new Path(locations.get("PL"), locations.get("EC")));
		paths.add(new Path(locations.get("LO"), locations.get("EC")));
		paths.add(new Path(locations.get("NA"), locations.get("BB")));
		paths.add(new Path(locations.get("LE"), locations.get("EC")));
		paths.add(new Path(locations.get("BO"), locations.get("BB")));
		paths.add(new Path(locations.get("SN"), locations.get("BB")));
		paths.add(new Path(locations.get("BA"), locations.get("MS")));
		paths.add(new Path(locations.get("AL"), locations.get("MS")));
		paths.add(new Path(locations.get("CA"), locations.get("AO")));
		paths.add(new Path(locations.get("GO"), locations.get("TS")));
		paths.add(new Path(locations.get("MS"), locations.get("TS")));
		paths.add(new Path(locations.get("MR"), locations.get("MS")));
		paths.add(new Path(locations.get("HA"), locations.get("NS")));
		paths.add(new Path(locations.get("AM"), locations.get("NS")));
		paths.add(new Path(locations.get("NS"), locations.get("EC")));


	}

}
