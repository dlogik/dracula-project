package dracula;

import java.util.ArrayList;

public class Location {
	String name;
	// Location type: "city" or "sea"
	String locationType;
	
	// List of paths from this location
	ArrayList<Path> incidentPaths;
	
	public Location(String name, String locationType) {
		this.name = name;
		this.locationType = locationType;
		incidentPaths = new ArrayList<Path>();
	}
	
	public void addPath(Path path) {
		incidentPaths.add(path);
	}


}
