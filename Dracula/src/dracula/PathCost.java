package dracula;


public class PathCost {

	final int INFINITY = 1000; // Only needs to be higher than the highest link cost
	int cost;
	Location predecessor, location;

	public PathCost(Location location, Location source) {
		this.location = location;
		Path path = DracMap.getPath(location, source);
		if (path == null) {
			predecessor = null;
			cost = INFINITY;
		}
		else {
			predecessor = source;
			cost = 1;
		}			
	}

	public void updateCost(PathCost leastCostLink) {
		Path path = DracMap.getPath(location, leastCostLink.location);
		if(path == null) {
			return;
		}

		if(leastCostLink.cost + 1 < cost) {
			predecessor = leastCostLink.location;
			cost = leastCostLink.cost + 1;
		}


	}

}
