package dracula;

public class Path {
	// References to the two end-point locations of this path
	Location location1, location2;
	
	
	public Path(Location location1, Location location2) {
		this.location1 = location1;
		this.location2 = location2;
		// Add this path to the list of incident paths in the two locations
		location1.addPath(this);
		location2.addPath(this);
	}

}
