/**
 * Written for COMP9024 2013s2 at UNSW Kensington.
 * @author dstacey@cse.unsw.edu.au
 *
 *  EDIT this file
 */

package dracula;

import dracula.Dracula;

public class PlayerFactory {

   public static Dracula getDracula(String pastPlays, String[] messages) {
	   // Instantiate dracula class.
      Dracula dracula = new MyDraculaOne(pastPlays, messages);
      
      return dracula;
   }

}
