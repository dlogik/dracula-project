// Sample map class designed by Emil Bigaj
// https://www.openlearning.com/u/emilbigaj/blog/ImplementationOfDraculaMap

package Tests;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SampleMap {

 // This is used for the map size, maxKey is the largest possible key that can be produced by the has key function
 int maxKey = 2627; //corresponds to ZZ + 1 = 2626+1
 // These represent each players position location
 private int G, S, H, M, D;
 // The last 6 places dracula has been
 private int[] trail = new int[6];
 // These adjacency matrix represent the game map
 private int[][] path = new int[maxKey][maxKey];
 private int[][] rail = new int[maxKey][maxKey];
 private int[][] sea = new int[maxKey][maxKey];

 public SampleMap() {
     addPath("CD", "GA");
     addPath("CD", "KL");
     addPath("KL", "BD");
     addPath("KL", "SZ");
     addPath("KL", "BE");
     addPath("KL", "BC");
     addPath("KL", "GA");
     addPath("GA", "CN");
     addPath("GA", "BC");
     addPath("BC", "CN");
     addPath("BC", "SO");
     addPath("BC", "BE");
     addPath("SO", "VR");
     addPath("SO", "SA");
     addPath("SO", "BE");
     addPath("SO", "SJ");
     addPath("SO", "VA");
     addPath("SZ", "BE");
     addPath("SZ", "BD");
     addPath("SZ", "ZA");
     addPath("SZ", "JM");
     addPath("BD", "VI");
     addPath("BD", "ZA");
     addPath("VI", "ZA");
     addPath("VI", "PR");
     addPath("VI", "MU");
     addPath("ZA", "MU");
     addPath("ZA", "SJ");
     addPath("ZA", "JM");
     addPath("CN", "VR");
     addPath("SA", "VA");
     addPath("VA", "AT");
     addPath("VA", "SJ");
     addPath("SJ", "JM");
     addPath("SJ", "BE");
     addPath("BE", "JM");
     addPath("VE", "MU");
     addPath("VE", "MI");
     addPath("VE", "FL");
     addPath("VE", "GO");
     addPath("FL", "GO");
     addPath("FL", "RO");
     addPath("RO", "BI");
     addPath("RO", "NP");
     addPath("NP", "BI");
     addPath("CF", "MR");
     addPath("CF", "TO");
     addPath("CF", "BO");
     addPath("CF", "NA");
     addPath("CF", "PA");
     addPath("CF", "GE");
     addPath("PA", "CF");
     addPath("PA", "NA");
     addPath("PA", "LE");
     addPath("PA", "BU");
     addPath("PA", "ST");
     addPath("PA", "GE");
     addPath("BU", "PA");
     addPath("BU", "LE");
     addPath("BU", "AM");
     addPath("BU", "CO");
     addPath("BU", "ST");
     addPath("AM", "BU");
     addPath("AM", "CO");
     addPath("HA", "BR");
     addPath("HA", "LI");
     addPath("HA", "CO");
     addPath("CO", "ST");
     addPath("CO", "BU");
     addPath("CO", "AM");
     addPath("CO", "HA");
     addPath("CO", "LI");
     addPath("CO", "FR");
     addPath("ST", "GE");
     addPath("ST", "PA");
     addPath("ST", "BU");
     addPath("ST", "CO");
     addPath("ST", "FR");
     addPath("ST", "NU");
     addPath("ST", "MU");
     addPath("ST", "ZU");
     addPath("GE", "CF");
     addPath("GE", "PA");
     addPath("GE", "ST");
     addPath("GE", "ZU");
     addPath("GE", "MR");
     addPath("MR", "TO");
     addPath("MR", "CF");
     addPath("MR", "GE");
     addPath("MR", "ZU");
     addPath("MR", "MI");
     addPath("MR", "GO");
     addPath("BR", "HA");
     addPath("BR", "LI");
     addPath("BR", "PR");
     addPath("LI", "NU");
     addPath("LI", "FR");
     addPath("LI", "CO");
     addPath("LI", "HA");
     addPath("LI", "BR");
     addPath("FR", "NU");
     addPath("FR", "ST");
     addPath("FR", "CO");
     addPath("FR", "LI");
     addPath("ZU", "MI");
     addPath("ZU", "MR");
     addPath("ZU", "GE");
     addPath("ZU", "ST");
     addPath("ZU", "MU");
     addPath("MI", "GO");
     addPath("MI", "MR");
     addPath("MI", "ZU");
     addPath("MI", "MU");
     addPath("MI", "VE");
     addPath("GO", "FL");
     addPath("GO", "MR");
     addPath("GO", "MI");
     addPath("GO", "VE");
     addPath("PR", "BR");
     addPath("PR", "NU");
     addPath("PR", "VI");
     addPath("NU", "MU");
     addPath("NU", "ST");
     addPath("NU", "FR");
     addPath("NU", "LI");
     addPath("NU", "PR");
     addPath("MU", "VE");
     addPath("MU", "MI");
     addPath("MU", "ZU");
     addPath("MU", "ST");
     addPath("MU", "NU");
     addPath("MU", "VI");
     addPath("MU", "ZA");
     addPath("ED", "MN");
     addPath("MN", "LV");
     addPath("MN", "LO");
     addPath("LO", "SW");
     addPath("LO", "PL");
     addPath("LV", "SW");
     addPath("DU", "GW");
     addPath("LE", "NA");
     addPath("LE", "PA");
     addPath("PA", "NA");
     addPath("NA", "CF");
     addPath("CF", "BO");
     addPath("CF", "TO");
     addPath("TO", "BO");
     addPath("BO", "SR");
     addPath("TO", "SR");
     addPath("TO", "BA");
     addPath("SR", "SN");
     addPath("SR", "AL");
     addPath("SR", "MA");
     addPath("SN", "MA");
     addPath("MA", "AL");
     addPath("MA", "LS");
     addPath("MA", "CA");
     addPath("MA", "GR");
     addPath("AL", "GR");
     addPath("LS", "SN");
     addPath("CA", "LS");
     addPath("CA", "GR");
     addPath("SR", "BA");

     // add the rails
     addRail("GA", "BC");
     addRail("BC", "CN");
     addRail("VR", "SO");
     addRail("SO", "SA");
     addRail("SO", "BE");
     addRail("BE", "SZ");
     addRail("SZ", "BC");
     addRail("SZ", "BD");
     addRail("BD", "VI");
     addRail("VI", "PR");
     addRail("VI", "VE");
     addRail("FL", "MI");
     addRail("FL", "RO");
     addRail("RO", "NP");
     addRail("NP", "BI");

     //add the sea
     addSea("CN", "BS");
     addSea("VR", "BS");
     addSea("SA", "IO");
     addSea("AT", "IO");
     addSea("VA", "IO");
     addSea("VE", "AS");
     addSea("BI", "AS");
     addSea("CG", "TS");
     addSea("RO", "TS");
     addSea("NP", "TS");
     addSea("CG", "MS");
     addSea("MS", "TS");
     addSea("IO", "TS");
     addSea("IO", "AS");
     addSea("ED", "NS");
     addSea("LS", "NS");
     addSea("LV", "AO");
     addSea("GW", "AO");
     addSea("DU", "IS");
     addSea("SW", "IS");
     addSea("PL", "EC");
     addSea("LO", "EC");
     addSea("NA", "BB");
     addSea("LE", "EC");
     addSea("BO", "BB");
     addSea("SN", "BB");
     addSea("BA", "MS");
     addSea("AL", "MS");
     addSea("CA", "AO");
     addSea("GO", "TS");
     addSea("MS", "TS");
     addSea("MR", "MS");
     addSea("HA", "NS");
     addSea("AM", "NS");
     addSea("NS", "EC");

     //add Rails
     addRail("GA", "BC");
     addRail("BC", "CN");
     addRail("VR", "SO");
     addRail("SO", "SA");
     addRail("SO", "BE");
     addRail("BE", "SZ");
     addRail("SZ", "BC");
     addRail("SZ", "BD");
     addRail("BD", "VI");
     addRail("VI", "PR");
     addRail("VI", "VE");
     addRail("FL", "MI");
     addRail("FL", "RO");
     addRail("RO", "NP");
     addRail("NP", "BI");
     addRail("ED", "MN");
     addRail("MN", "LO");
     addRail("MN", "LV");
     addRail("LO", "SW");
     addRail("LE", "PA");
     addRail("BO", "PA");
     addRail("BO", "SR");
     addRail("SR", "BA");
     addRail("SR", "MA");
     addRail("MA", "SN");
     addRail("MA", "AL");
     addRail("MA", "LS");
     addRail("BC", "CN");
     addRail("VR", "SO");
     addRail("SO", "SA");
     addRail("SO", "BE");
     addRail("BE", "SZ");
     addRail("SZ", "BC");
     addRail("SZ", "BD");
     addRail("BD", "VI");
     addRail("VI", "PR");
     addRail("VI", "VE");
     addRail("FL", "MI");
     addRail("FL", "RO");
     addRail("RO", "NP");
     addRail("NP", "BI");

 }

 // sets the trail of dracula
 public void setTrail(String[] newTrail) {
     for (int i = 0; i <= newTrail.length - 1; i++) {
         trail[i] = getKey(newTrail[i]);
     }
 }

 // sets the location of a player on the map
 public void setPlayerLocation(String player, String Location) {
     int key = getKey(Location);
     switch (player) {
         case "G":
             G = key;
         case "S":
             S = key;
         case "H":
             H = key;
         case "M":
             M = key;
         case "D":
             D = key;
     }
 }

 // adds a path to the map from location1 to location2
 public void addPath(String Location1, String Location2) {
     path[getKey(Location1)][getKey(Location2)] = 1;
     path[getKey(Location2)][getKey(Location1)] = 1;
 }

 // adds a rail to the map from location1 to location2
 public void addRail(String Location1, String Location2) {
     rail[getKey(Location1)][getKey(Location2)] = 1;
     rail[getKey(Location2)][getKey(Location1)] = 1;
 }

 // adds a sea connection from location1 to location2
 public void addSea(String Location1, String Location2) {
     sea[getKey(Location1)][getKey(Location2)] = 1;
     sea[getKey(Location2)][getKey(Location1)] = 1;
 }

 // checks if this location is free for Dracula to move to
 public boolean isLocationFree(int key) {
     if (G == key || S == key || H == key || M == key) {
         return false;
     } else {
         return true;
     }
 }

 // returns the location of this character
 public int whereIs(String character) {
     switch (character) {
         case "G":
             return G;
         case "S":
             return S;
         case "H":
             return H;
         case "M":
             return M;
         case "D":
             return D;
     }
     return -1;
 }
 
 // tells you where you can move to from this location via what movement type
 // mapType is "path", "rail" or "sea"
 public int[] moveFrom(String mapType, int key) {
     int moveHere[] = new int[10];
     int possibleMoves = 0;
     for (int i = 1; i < maxKey; i++) {
         if (mapType.equals("path")) {
             if (path[key][i] == 1) {
                 moveHere[possibleMoves++] = i;
             }
         } else if (mapType.equals("rail")) {
             if (rail[key][i] == 1) {
                 moveHere[possibleMoves++] = i;
             }
         } else if (mapType.equals("sea")) {
             if (sea[key][i] == 1) {
                 moveHere[possibleMoves++] = i;
             }
         }
     }
     int[] newMoveHere = new int[possibleMoves];
     for (int i = 0; i < possibleMoves; i++) {
         newMoveHere[i] = moveHere[i];
  }
     return newMoveHere;
 }
 
 // put in a string and it will be turned into a hash key thats used for the map
 public static int getKey(String location) {
     int key = 0;
     key = key + (location.charAt(0) - 64) * 100;
     key = key + (location.charAt(1) - 64);
     return key;
 }

 // put in a key and it will return a string corresponding to a location
 public static String getLocation(int key) {
     char letter1, letter2;
     String location;
     letter1 = (char) (key % 100 + 64);
     letter2 = (char) (key / 100 + 64);
     location = "" + letter2 + letter1;
     return location;
 }

 // print the map you want
 public void printMe(String printThis) {
     switch (printThis) {
         case "path":
             printMap(path);
             break;
         case "rail":
             printMap(rail);
             break;
         case "sea":
             printMap(sea);
             break;
     }
 }

 // prints a map
 public void printMap(int[][] map) {
     for (int i = 1; i < maxKey; i++) {
         for (int j = 1; j < maxKey; j++) {
             if (map[i][j] == 1) {
                 System.out.print(getLocation(i) + " | " + getLocation(j));
                 System.out.print(", map[" + i + "][" + j + "] = " + map[i][j]);
                 System.out.println(", map[" + j + "][" + i + "] = " + map[j][i]);
             }
         }
     }
 }
}