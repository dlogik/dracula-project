package Tests;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import dracula.DracMap;
import dracula.Dracula;
import dracula.DraculaMove;
import dracula.PastPlay;
import dracula.PlayerFactory;

public class UnitTests {
	
	// Tests the past plays class. These tests use JUnit in eclipse.
	/**
	 * 
	 */
	@Test
	public void TestPastPlays() {
		String move = "DSJ.V..";
		
		PastPlay pastPlay = new PastPlay(move);
		
		assertEquals(pastPlay.getCharacterCode(), 'D');
		assertEquals(pastPlay.getCurrentCityCode(), "SJ");
		
		move = "DD5.V..";
		
		pastPlay = new PastPlay(move);
		
		assertEquals(pastPlay.getCharacterCode(), 'D');
		assertEquals(pastPlay.getCurrentCityCode(), "D5");
		assertTrue(pastPlay.doubleBackMove());
		assertEquals(pastPlay.doubleBackNumber(), 5);
		assertFalse(pastPlay.trapPlaced());
		assertTrue(pastPlay.vampirePlaced());
		assertFalse(pastPlay.trapLeft());
		assertFalse(pastPlay.vampireMatured());
		
		move = "DSW.V..";
		
		pastPlay = new PastPlay(move);
		
		assertEquals(pastPlay.getCurrentCityCode(), "SW");
	}
	
	// Tests that Dracula is moving correctly.
	@Test
	public void TestGetString() {
		
		// Dracula will be at 
		String pastPlays = "GKL.... SKL.... HCD.... MGA.... DSR.V.. GCD.... SCD.... HGA.... MKL....";
		String[] messages = new String[1];		
		messages[0] = "Test message";
		
		   // Instantiate dracula class.
	    Dracula dracula = PlayerFactory.getDracula(pastPlays, messages);

		DraculaMove decideMove = dracula.decideMove();	
		String playString = decideMove.getPlayAsString();
		
		// From SR, Dracula can go to: SN, AL or MA via road. 
		List<String> validMoves = Arrays.asList("SN", "AL", "MA", "BO", "TO", "BA");
		
		// Check if our returned string contains one of the valid cities.
		assertTrue(validMoves.contains(playString));
	}
	
	// Prints out the map.
	@Test
	public void TestPrintMap() {
		DracMap map = new DracMap();
		
		map.printMap();
	}
	
}
